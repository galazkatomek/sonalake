import AirportWrapperTemplate from './airport-wrapper.component.html';
import AirportWrapperController from './airport-wrapper.controller';
import AirportWrapperComponent from './airport-wrapper.component';
import componentsModuleName from '../components'



describe('AirportWrapperComponent', () => {
	let $compile;
	let $rootScope;
	let componentController;


	var AirportsService = {
		getAirports: function () {
			return Promise.resolve([]);
		}
	}

	beforeEach(angular.mock.module(componentsModuleName));

	beforeEach(() => {
		angular.mock.module(function ($provide) {
			$provide.value('AirportsService', AirportsService);
		});

		inject((_$compile_, _$rootScope_, $controller, $componentController) => {
			$compile = _$compile_;
			$rootScope = _$rootScope_;
			componentController = $componentController
		})
	});


	describe('Component', () => {
		it('compile component', () => {
			var element = $compile("<airport-wrapper></airport-wrapper>")($rootScope);
			$rootScope.$digest();
			expect(element.html()).contain('<div>');


		});
	});
});
