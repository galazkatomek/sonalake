export default class AirportWrapperController {
	constructor() {
		'ngInject';
	}

	/**
	 * Triggered from child component
	 */
	onAirportStartChangeFromChild(airport) {
		this.airportStart = airport; // use to send data to child node with airport dest
		this.onAirportStartChange({ airport: airport });// use to send data to parent
	}

	/**
	 * Triggered from child component
	 */
	onAirportDestChangeFromChild(airport) {
		this.airportDest = airport; // use to send data to child node with airport start
		this.onAirportDestChange({ airport: airport });// use to send data to parent
	}

}
