import AirportSelectorTemplate from './airport-selector.component.html';
import AirportSelectorController from './airport-selector.controller';
import AirportSelectorComponent from './airport-selector.component';
import componentsModuleName from '../components'


describe('AirportSelector', () => {
	let makeController;
	let $compile;
	let $rootScope;
	let componentController;
	let q;


	let AirportsServiceMock = {
		getAirports: function () {
			return Promise.resolve([]);
		}
	}

	beforeEach(angular.mock.module(componentsModuleName));

	beforeEach(() => {

		angular.mock.module(function ($provide) {
			$provide.value('AirportsService', AirportsServiceMock);
		});

		inject((_$compile_, _$rootScope_, $controller, $componentController, $q) => {
			$compile = _$compile_;
			$rootScope = _$rootScope_;
			componentController = $componentController
			makeController = () => {
				return new AirportSelectorController(AirportsServiceMock);
			};
		})
	});

	describe('Controller', () => {
		// check controller 
		it('has a airports property', () => {
			let controller = makeController();
			expect(controller).to.have.property('airports');
		});

	});


	describe('Component', () => {
		it('compile component', () => {
			// component/directive specs
			var element = $compile("<airport-selector></airport-selector>")($rootScope);
			$rootScope.$digest();
			// Check that the compiled element contains the templated content
			expect(element.html()).contain('</div>');
			expect(element.html()).contain('limitTo: $ctrl.AIRPORT_LIMIT_TO_SHOW');

		});

		it('to have property airportUserInput', () => {
			var airportSelector = componentController('airportSelector', null, {});
			expect(airportSelector).to.have.property('airportUserInput');
		});
	});
});
