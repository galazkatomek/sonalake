import template from './airport-selector.component.html';
import controller from './airport-selector.controller';
import './airport-selector.component.scss'
export const AirportSelectorComponent = {
  restrict: 'E',
  bindings: {
    onAirportChange: '&',
    airportFromAnother: '<',
  },
  template,
  controller
};
