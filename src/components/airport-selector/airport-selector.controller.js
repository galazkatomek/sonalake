export default class AirportSelectorController {
	constructor(AirportsService) {
		'ngInject';
		this.airportUserInput = '';
		this.AIRPORT_LIMIT_TO_SHOW = 5; // const - how many airports show in type-ahead
		this.airportSelectedIndex = -1;
		this.showTypeAhead = false;
		this.airportsService = AirportsService;

		this.airports = null;// list of all airports
		this.airportsFiltered = null;// list of filtered airports by user input
		this.airportsService.getAirports().then(data => {
			this.airports = data;
			this.airportsFiltered = data;
		});
	}

	$onChanges(changes) {
		if (changes.airportFromAnother) {
			this.onInputChange();
		}
	}

	onInputChange() {
		this.showTypeAhead = !!this.airportUserInput;
		if (!this.showTypeAhead || !this.airports) {
			return;
		}
		const input = this.airportUserInput.toLowerCase();
		this.airportsFiltered = this.airports.filter(airport => {
			if (this.airports && this.airportFromAnother === airport) {
				return false; // block possibility to select the same airport like in other controller
			}
			// filter by airport: name, iataCode and country.name
			return ((airport.name.toLowerCase().indexOf(input) > -1)
				|| (airport.iataCode.toLowerCase().indexOf(input)) > -1 || (airport.country.name.toLowerCase().indexOf(input) > -1));
		});
	}

	selectAirport(airport) {
		this.airportUserInput = `${airport.name} (${airport.iataCode}) - ${airport.country.name}`;
		this.onAirportChange({ airport: airport });// notify parent about change
		this.showTypeAhead = false;
	}

	keyEvent(e) {
		if (!this.airportUserInput || !this.airportsFiltered || !this.airportsFiltered.length) {
			return;
		}
		switch (e.keyCode) {
			case 13: // enter
				if (this.airportsFiltered[this.airportSelectedIndex]) {
					this.selectAirport(this.airportsFiltered[this.airportSelectedIndex]);
				}
				break;
			case 38: //arrow up 
				this.airportSelectedIndex--;
				if (this.airportSelectedIndex < 0) {
					this.airportSelectedIndex = (this.AIRPORT_LIMIT_TO_SHOW - 1);
				}
				break;
			case 40: // arrow down
				this.airportSelectedIndex++;
				if (this.airportSelectedIndex >= this.AIRPORT_LIMIT_TO_SHOW) {
					this.airportSelectedIndex = 0;
				}
				break;
			default:
				break;
		}
	}
}
