import moment from 'moment';

export default class DateWrapperController {
  constructor() {

  }


  onStartDateChangeFromChild(date) {
    this.startDate = date;
    this.onStartDateChange({ date: date });
    if (moment(this.startDate) > moment(this.endDate)) {
      this.newEndDateOut = moment(this.startDate).add(2, 'd').toDate();
    }
  }
  onEndDateChangeFromChild(date) {
    this.endDate = date;
    this.onEndDateChange({ date: date });
    if (moment(this.endDate) < moment(this.startDate)) {
      this.newStartDateOut = moment(this.endDate).subtract(2, 'd').toDate();
    }
  }


}

