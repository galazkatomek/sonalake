import template from './date-wrapper.component.html';
import controller from './date-wrapper.controller';

export const DateWrapperComponent = {
  restrict: 'E',
  bindings: {
    onStartDateChange: '&',
    onEndDateChange: '&'
  },
  template,
  controller
};
