import DateSelectorComponentTemplate from './date-selector.component.html';
import DateSelectorController from './date-selector.controller';
import DateSelectorComponent from './date-selector.component';
import componentsModuleName from '../components'


describe('DateSelector', () => {
	let makeController;
	let $compile;
	let $rootScope;
	let componentController;

	beforeEach(angular.mock.module(componentsModuleName));

	beforeEach(() => {
		angular.mock.module(function ($provide) {
			// $provide.value('AirportsService', AirportsServiceMock);
		});

		inject((_$compile_, _$rootScope_, $controller, $componentController, $q) => {
			$compile = _$compile_;
			$rootScope = _$rootScope_;
			componentController = $componentController
			makeController = () => {
				return new DateSelectorController();
			};
		})
	});

	describe('Controller', () => {
		// check controller 
		it('has a dateFromParent property', () => {
			let controller = makeController();
			expect(controller).to.not.have.property('dateFromParent');
		});
	});


	describe('Component', () => {
		it('compile component', () => {
			var element = $compile("<date-selector></date-selector>")($rootScope);
			$rootScope.$digest();
			expect(element.html()).contain('<input');
		});

		it('check bindings: dateFromParent', () => {
			inject(function ($compile, $rootScope) {
				var parentScope = $rootScope.$new();
				parentScope.dateFromParent = new Date();
				// on-date-change="$ctrl.onStartDateChange(date)"
				var element = angular.element('<date-selector date-from-parent="dateFromParent">');

				var compiledElement = $compile(element)(parentScope);
				parentScope.$digest();
				var scope = compiledElement.isolateScope();
				expect(scope.$ctrl.dateFromParent).to.equal(parentScope.dateFromParent, 'dateFromParent should change');
				expect(scope.$ctrl.date).to.equal(parentScope.dateFromParent, 'date should change');

				scope.$ctrl.date = new Date();
				parentScope.$digest();
				expect(scope.$ctrl.date).to.not.equal(parentScope.dateFromParent, 'ctrl.date should not change dateFromParent: wrong bindings');
			});

		});

	});
});
