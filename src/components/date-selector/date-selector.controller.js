export default class DateSelectorController {
	constructor() {
	}

	$onChanges(changes) {
		if (changes.dateFromParent && this.dateFromParent) {
			this.date = this.dateFromParent;
			this.onDateChange({ date: this.date });
		}
	}

}



