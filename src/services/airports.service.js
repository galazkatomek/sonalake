export class AirportsService {

  constructor($http, $q) {
    'ngInject';
    this.$http = $http;
    this.$q = $q;
    this.flightsData = null;
    this.init();
  }

  loadAirports() {
    const AIRPORT_CODES_URL = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';
    return this.$http({
      method: 'GET',
      url: AIRPORT_CODES_URL
    }).then(data => data.data)
  }

  getAirports() {
    return this.getFlightsData().then(data => data.airports);
  }
  getRoutes() {
    return this.getFlightsData().then(data => data.routes);
  }

  getFlightsData() {
    if (this.flightsData) {
      var deferred = this.$q.defer();
      deferred.resolve(this.flightsData);
      return deferred.promise;
    }
    return this.airportsPromise;
  }

  init() {
    const AIRPORT_CODES_STORAGE_KEY = 'ryanair.airportCodes';
    const airportsDeferred = this.$q.defer();
    this.airportsPromise = airportsDeferred.promise;

    var flightsFromCache = localStorage.getItem(AIRPORT_CODES_STORAGE_KEY);
    if (flightsFromCache) {
      this.flightsData = JSON.parse(flightsFromCache);
      airportsDeferred.resolve(this.flightsData);
      return;
    }

    this.loadAirports()
      .then(data => {
        localStorage.setItem(AIRPORT_CODES_STORAGE_KEY, JSON.stringify(data));
        this.flightsData = data;
        airportsDeferred.resolve(data);
      }).catch(error => {
        airportsDeferred.reject(error);
      });
  };


};
