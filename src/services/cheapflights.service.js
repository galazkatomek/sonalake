export class CheapFlightService {

  constructor($http) {
    'ngInject';
    this.$http = $http;
  }

  getCheapFlights(startIataCode, endIataCode, startDateAsString, endDateAsString) {
    const CHEAP_FLIGHT_URL = `https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/from/${startIataCode}/to/${endIataCode}/${startDateAsString}/${endDateAsString}/250/unique/?limit=15&offset-0`;
    return this.$http({
      method: 'GET',
      url: CHEAP_FLIGHT_URL
    }).then(data => data.data)
  }
};
