import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Components from './components/components';
import { HomeComponent } from './home/home.component';
import { FlightsListComponent } from './home/flights-list/flights-list.component';

import {
  CheapFlightService,
  AirportsService
} from './services';

angular.module('myApp', [
  uiRouter,
  Components
])
  .component('homePage', HomeComponent)
  .component('flightsList', FlightsListComponent)
  .service('AirportsService', AirportsService)
  .service('CheapFlightService', CheapFlightService)
  .config(($stateProvider, $locationProvider) => {
    'ngInject';

    $locationProvider.hashPrefix('');
    $stateProvider
      .state('home', {
        url: '',
        template: '<home-page></home-page>'
      }).state('flights', {
        url: '/flights/from/:startIataCode/to/:endIataCode/:startDate/:endDate',
        template: '<flights-list></flights-list>',
        parent: 'home'
      });
  });
