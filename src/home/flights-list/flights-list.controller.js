export default class FlightsListController {
	constructor(CheapFlightService, $stateParams) {
		'ngInject';
		this.$stateParams = $stateParams;
		this.cheapFlightService = CheapFlightService;
	}

	$onInit() {
		let startIataCode = this.$stateParams.startIataCode;
		let endIataCode = this.$stateParams.endIataCode;
		let startDate = this.$stateParams.startDate;
		let endDate = this.$stateParams.endDate;

		this.cheapFlightService.getCheapFlights(
			startIataCode, endIataCode, startDate, endDate).then(data => {
				this.flights = data.flights;
				this.isLoading = false;
			}).catch(error => {
				this.isLoading = false;
				alert(error);
			});
	}


}
