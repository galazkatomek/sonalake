import template from './flights-list.component.html';
import controller from './flights-list.controller';
export const FlightsListComponent = {
  restrict: 'E',
  template,
  controller
};
