import template from './home.component.html';
import controller from './home.controller';
import './home.component.scss';

export const HomeComponent = {
  restrict: 'E',
  scope: {},
  template,
  controller,
  bindToController: true
};
