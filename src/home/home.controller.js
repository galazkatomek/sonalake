import moment from 'moment';

export default class HomeController {
	constructor(CheapFlightService, $state) {
		'ngInject';
		this.name = 'home';
		this.cheapFlightService = CheapFlightService;
		this.$state = $state;
		this.isLoading = false;
	}

	canSearch() {
		if (this.isLoading) {
			return false;
		}
		return (this.startDate && this.endDate && this.airportStart && this.airportDest);
	}

	searchCheapFlights() {
		// this.isLoading = true;

		const DATE_FORMAT = 'YYYY-MM-DD';
		const startDateAsString = moment(this.startDate).format(DATE_FORMAT);
		const endDateAsString = moment(this.endDate).format(DATE_FORMAT);
		const startIataCode = this.airportStart.iataCode;
		const endIataCode = this.airportDest.iataCode;

		this.$state.go('flights', { startIataCode, endIataCode, startDate: startDateAsString, endDate: endDateAsString });
	}


	onStartDateChangeFromChild(date) {
		this.startDate = date;
	}

	onEndDateChangeFromChild(date) {
		this.endDate = date;
	}

	/**
	 * Triggered from child component
	 */
	onAirportStartChangeFromChild(airport) {
		this.airportStart = airport;
	}

	/**
	 * Triggered from child component
	 */
	onAirportDestChangeFromChild(airport) {
		this.airportDest = airport;
	}


}